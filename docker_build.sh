# File: avc-examples-db3/docker_build.sh

set -e
    
# Update tableNamePrefix from "xxx_" to "it_"

sed -i s/xxx_/it_/g create_tables.sql

# Build a temporary image

docker build -t tmp/examples-db3 .

# Run a temporary container

docker container run -d -it --name tmp-db3 tmp/examples-db3

docker ps

# Wait for the SQL to be executed

echo "Sleeping 10 sec..."
sleep 10

docker ps

echo
echo "logs:"
docker logs --tail 200 tmp-db3

# Delete the SQL file from the running container since it's already imported
#
# docker container exec tmp-db3 rm -rf /docker-entrypoint-initdb.d/*
docker container exec tmp-db3 touch /docker-entrypoint-initdb.d/create_tables.sql

# Commit these changes to the target image

docker container commit tmp-db3 avcompris/examples-db3

# Stop the running container

docker container rm -f tmp-db3

-- File: avc-examples-db3/create_tables.sql

CREATE TABLE xxx_correlations (
	correlation_id VARCHAR(255) NOT NULL,
	created_at TIMESTAMPTZ NOT NULL,
	PRIMARY KEY (correlation_id)
);

CREATE TABLE xxx_users (
	username VARCHAR(255) NOT NULL,
	rolename VARCHAR(255) NOT NULL,
	preferred_lang VARCHAR(10) NULL,
	preferred_timezone VARCHAR(10) NULL,
	enabled BOOLEAN NOT NULL,
	created_at TIMESTAMPTZ NOT NULL,
	updated_at TIMESTAMPTZ NOT NULL,
	revision INTEGER NOT NULL,
	last_active_at TIMESTAMPTZ NULL,
	PRIMARY KEY (username)
);

CREATE TABLE xxx_auth (
	username VARCHAR(255) NOT NULL,
	password_salt VARCHAR(255) NOT NULL,
	password_hash VARCHAR(255) NOT NULL,
	PRIMARY KEY (username),
	FOREIGN KEY (username) REFERENCES xxx_users (username) ON DELETE CASCADE
);

CREATE TABLE xxx_auth_sessions (
	user_session_id VARCHAR(255) NOT NULL,
	username VARCHAR(255) NOT NULL,
	created_at TIMESTAMPTZ NOT NULL,
	updated_at TIMESTAMPTZ NOT NULL,
	expires_at TIMESTAMPTZ NOT NULL,
	expired_at TIMESTAMPTZ NULL,
	PRIMARY KEY (user_session_id),
	FOREIGN KEY (username) REFERENCES xxx_users (username) ON DELETE CASCADE
);

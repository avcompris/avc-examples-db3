# avc-examples-db3

Docker image: avcompris/examples-db3

Usage:

	$ docker run -d --name examples-db3 \
	    -p 5432:5432 \
	    avcompris/examples-db3


Exposed port is 5432.

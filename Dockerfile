# File: avc-examples-db3/Dockerfile
#
# Use to build the image: avcompris/examples-db3

FROM postgres:9.6
MAINTAINER david.andriana@avantage-compris.com

#-------------------------------------------------------------------------------
#   1. POSTGRES
#-------------------------------------------------------------------------------

ENV POSTGRES_USER     postgres
ENV POSTGRES_PASSWORD root123
ENV POSTGRES_DB       db3

# PGDATA=/data, otherwise, /var/lib/postgresql/data will be used (and empty)
#
ENV PGDATA=/data

#-------------------------------------------------------------------------------
# 2. SQL
#-------------------------------------------------------------------------------

COPY create_tables.sql /docker-entrypoint-initdb.d/

#-------------------------------------------------------------------------------
# 8. BUILDINFO (RELY ON JENKINS: POPULATED VIA "buildinfo.sh")
#-------------------------------------------------------------------------------

COPY buildinfo /

#-------------------------------------------------------------------------------
#   9. END
#-------------------------------------------------------------------------------

EXPOSE 5432
